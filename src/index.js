'use strict';

const assert = require('assert');
const duplexify = require('duplexify');
const mergeOptions = require('merge-options');
const stream = require('stream');

const chunkStreamDuplex = function (chunkToStream, options) {
	const concurrency = options.concurrency || 1;
	let requestMore = null;
	let hasEnded = false;
	let chunkStreams = [];

	if (typeof chunkToStream !== 'function') {
		// Generate a TypeError:
		chunkToStream();
	}

	const add = (chunkStream) => {
		chunkStreams.push(chunkStream);

		chunkStream.once('end', () => {
			remove(chunkStream);
		});

		chunkStream.once('error', (error) => {
			signalError(error);
		});

		chunkStream.pipe(destination, {
			end: false
		});
	};

	const signalError = (error) => {
		destination.emit('error', error);
	};

	const remove = (chunkStream) => {
		chunkStreams = chunkStreams.filter((item) => {
			return item !== chunkStream;
		});

		if (hasEnded && !chunkStreams.length && destination.readable) {
			destination.end();
		}

		if (chunkStreams.length < concurrency) {
			const more = requestMore;
			if (more) {
				requestMore = null;
				more();
			}
		}
	};

	const destination = new stream.PassThrough(mergeOptions(
		{
			objectMode: true
		},
		options
	));

	const source = new stream.Writable(mergeOptions(
		{
			objectMode: true,

			write(chunk, encoding, callback) {
				try {
					const chunkStream = chunkToStream(chunk);

					add(chunkStream);

					if (chunkStreams.length >= concurrency) {
						requestMore = callback;
					} else {
						callback();
					}
				} catch (error) {
					callback(error);
				}
			}
		},
		options
	));

	source.on('finish', () => {
		assert(!requestMore, 'Writable finished with more to read.');

		hasEnded = true;

		if (chunkStreams.length === 0) {
			destination.end();
		}
	});

	destination.setMaxListeners(0);
	destination.on('unpipe', remove);

	return duplexify.obj(source, destination, options);
};

module.exports = (chunkToStream, options = {}) => {
	return chunkStreamDuplex(chunkToStream, options);
};

