'use strict';

const stream = require('stream');
const streamFromArray = require('stream-from-array');
const streamFromIterator = require('stream-from-iterator');
const streamFromPromise = require('stream-from-promise');
const tap = require('tap');
const throughStream = require('../src/index');

const readImmediate = (chunks) => {
	return streamFromArray(chunks, {
		objectMode: true,
		highWaterMark: 2
	});
};

const readOneEvery = (ms, chunks) => {
	let index = 0;
	let pushedIndex = 0;
	let accepting = false;
	let done = false;

	const pushChunksWhileAccepting = (readable) => {
		while (pushedIndex < index && accepting) {
			accepting = readable.push(chunks[pushedIndex++]);
		}

		if (pushedIndex === index && done) {
			readable.push(null);
		}
	};

	const readable = new stream.Readable({
		objectMode: true,
		highWaterMark: 2,

		read() {
			accepting = true;
			pushChunksWhileAccepting(this);
		}
	});

	const interval = setInterval(() => {
		++index;
		if (index === chunks.length) {
			clearInterval(interval);
			done = true;
		}

		if (accepting) {
			pushChunksWhileAccepting(readable);
		}
	}, ms);

	return readable;
};

const repeatGenerator = function* (number, times) {
	while (times--) {
		yield number;
	}
};

const repeatOneEvery = (ms, number, times) => {
	const generator = repeatGenerator(number, times);
	const chunks = Array.from(generator);

	return readOneEvery(ms, chunks);
};

const repeatImmediate = (number, times) => {
	const generator = repeatGenerator(number, times);

	return streamFromIterator(generator, {
		objectMode: true,
		highWaterMark: 2
	});
};

const pushImmediate = (target) => {
	return new stream.Writable({
		objectMode: true,
		highWaterMark: 2,

		write(chunk, encoding, callback) {
			target.push(chunk);
			callback();
		}
	});
};

const pushOneEvery = (ms, target) => {
	return new stream.Writable({
		objectMode: true,
		highWaterMark: 2,

		write(chunk, encoding, callback) {
			setTimeout(() => {
				target.push(chunk);
				callback();
			}, ms);
		}
	});
};

tap.test('Immediate read stream', (t) => {
	const expected = ['a', 'b', 'c'];
	const actual = [];

	const subject = readImmediate(
		expected
	);

	t.plan(1);

	subject.on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Immediate write stream', (t) => {
	const expected = ['a', 'b', 'c'];
	const actual = [];

	const subject = pushImmediate(actual);

	t.plan(1);

	readImmediate(
		expected
	).pipe(
		subject
	).on('finish', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Delayed read stream', (t) => {
	const expected = ['a', 'b', 'c'];
	const actual = [];

	const subject = readOneEvery(
		10,
		expected
	);

	t.plan(1);

	subject.on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Delayed write stream', (t) => {
	const expected = ['a', 'b', 'c'];
	const actual = [];

	const subject = pushOneEvery(10, actual);

	t.plan(1);

	readImmediate(
		expected
	).pipe(
		subject
	).on('finish', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'a'];
	const actual = [];

	const subject = repeatImmediate(
		'a',
		4
	);

	t.plan(1);

	subject.on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Delayed repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'a'];
	const actual = [];

	const subject = repeatOneEvery(
		10,
		'a',
		4
	);

	t.plan(1);

	subject.on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual, expected);
	});
});

tap.test('Immediate read through immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual.sort(), expected);
	});
});


tap.test('Immediate read through delayed repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(10, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read through immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		10,
		['a', 'b', 'c']
	).pipe(
		subject
	).on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read through delayed repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(5, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		10,
		['a', 'b', 'c']
	).pipe(
		subject
	).on('data', (chunk) => {
		actual.push(chunk);
	}).on('end', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Immediate read to immediate write through immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushImmediate(actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Immediate read to immediate write through delayed repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(10, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushImmediate(actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read to immediate write through immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		10,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushImmediate(actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read to immediate write through delayed repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(5, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		10,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushImmediate(actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Immediate read to delayed write through immediate repeat stream', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushOneEvery(10, actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});


tap.test('Delayed read to delayed write through immediate repeat stream (backpressure)', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		5,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushOneEvery(20, actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read to delayed write through delayed repeat stream (backpressure)', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(5, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		5,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushOneEvery(20, actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read to delayed write through immediate repeat stream (starvation)', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		20,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushOneEvery(5, actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Delayed read to delayed write through delayed repeat stream (starvation)', (t) => {
	const expected = ['a', 'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c'];
	const actual = [];

	const subject = throughStream((chunk) => {
		return repeatOneEvery(10, chunk, 3);
	}, {
		highWaterMark: 2,
		concurrency: 2
	});

	t.plan(1);

	readOneEvery(
		20,
		['a', 'b', 'c']
	).pipe(
		subject
	).pipe(
		pushOneEvery(5, actual)
	).on('finish', () => {
		t.deepEquals(actual.sort(), expected);
	});
});

tap.test('Creating through stream with non-function', (t) => {
	t.plan(1);

	t.throws(() => {
		throughStream('Not a function').write('a');
	}, TypeError);
});


tap.test('Exception in callback should yeild stream error', (t) => {
	const subject = throughStream(() => {
		throw new Error('Failure in callback');
	});

	t.plan(2);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('error', (e) => {
		t.ok(e instanceof Error, 'Expected Error object');
		t.equals(e.message, 'Failure in callback');
	}).resume(); // Need to kick-start the stream since we have no data handler.
});


tap.test('Exception in callback should end stream', (t) => {
	const subject = throughStream(() => {
		throw new Error('Child stream failed.');
	});

	t.plan(2);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('error', () => {
		// Need to handle error to prevent it propagating out of the stream:
		t.pass('Failing stream signaled error');
	}).on('end', () => {
		t.pass('Failing stream ended');
	}).resume(); // Need to kick-start the stream since we have no data handler.
});


tap.test('Error in child stream should yeild stream error', (t) => {
	const subject = throughStream(() => {
		return streamFromPromise(new Promise(() => {
			throw new Error('Child stream failed.');
		}));
	});

	t.plan(2);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('error', (e) => {
		t.ok(e instanceof Error, 'Expected Error object');
		t.equals(e.message, 'Child stream failed.');
	}).resume(); // Need to kick-start the stream since we have no data handler.
});


tap.test('Error in child stream should end stream', (t) => {
	const subject = throughStream(() => {
		return streamFromPromise(new Promise(() => {
			throw new Error('Child stream failed.');
		}));
	});

	t.plan(2);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('error', () => {
		// Need to handle error to prevent it propagating out of the stream:
		t.pass('Failing stream signaled error');
	}).on('end', () => {
		t.pass('Failing stream ended');
	}).resume(); // Need to kick-start the stream since we have no data handler.
});


tap.test('Error in child stream should be fatal', (t) => {
	const expected = ['a'];
	const actual = [];

	const subject = throughStream((chunk) => {
		if (chunk === 'b') {
			return streamFromPromise.obj(new Promise(() => {
				throw new Error('Child stream failed.');
			}));
		}

		return streamFromArray.obj([chunk]);
	});

	t.plan(3);

	readImmediate(
		['a', 'b', 'c']
	).pipe(
		subject
	).on('data', (chunk) => {
		actual.push(chunk);
	}).on('error', (e) => {
		t.ok(e instanceof Error, 'Expected Error object');
		t.equals(e.message, 'Child stream failed.');
	}).on('end', () => {
		t.deepEquals(actual.sort(), expected);
	});
});


tap.test('Immediate read through delayed repeat stream', (t) => {
	const concurrency = 1;
	let concurrentStreams = 0;

	const subject = throughStream((chunk) => {
		concurrentStreams += 1;

		t.ok(concurrentStreams <= concurrency, 'Less active streams than concurrency limit.');

		return repeatImmediate(chunk, 3).on('end', () => {
			concurrentStreams -= 1;
		});
	}, {
		concurrency: concurrency
	});

	t.plan(6);

	const actual = [];
	readImmediate(
		['a', 'b', 'c', 'd', 'e', 'f']
	).pipe(
		subject
	).pipe(
		pushOneEvery(5, actual)
	);
});



/*
tap.test('Through stream obeys highWaterMark', (t) => {
	t.plan(14);

	const subject = throughStream((chunk) => {
		return repeatImmediate(chunk, 6);
	}, {
		highWaterMark: 2,
		concurrency: 1
	});

	subject.on('end', () => {
		t.fail('Stream ended prematurely.');
	});
	subject.on('readable', () => {
		console.log('NOW READABLE');
	});

	subject.once('readable', () => {
		t.equals(subject.read(), 'a');
		t.equals(subject.read(), 'a');
		t.equals(subject.read(), 'a');
		t.equals(subject.read(), 'a');
		t.equals(subject.read(), 'a');
		console.log('READ a');
		t.equals(subject.read(), 'a');
		console.log('READ null');
		t.equals(subject.read(), null);
		console.log('WAITING READABLE');

		subject.once('readable', () => {
			console.log('READ b');
			t.equals(subject.read(), 'b');

			console.log('READ b');
			t.equals(subject.read(), 'b');

			console.log('WRITE d');
			t.ok(subject.write('d'), 'highWaterMark not reached');

			console.log('WRITE e');
			t.notOk(subject.write('e'), 'highWaterMark reached');
		});
	});

	t.ok(subject.write('a'), 'highWaterMark not reached');
	t.ok(subject.write('b'), 'highWaterMark not reached');
	t.notOk(subject.write('c'), 'highWaterMark reached');
});
*/
